#ifndef GAME_H
#define GAME_H

#include <irrlicht.h>
#include "GameGUI.h"
#include "MyEventReceiver.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>


using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class Game
{
    public:
        Game();
        virtual ~Game();

        void run();

        void debutJeu();
        void joueLettre();

        GameGUI *gui;

        int nbEssais;
        bool *choixLettreGagnant;
        int idMotMystere;
        std::string motMystere;


    protected:

    private:
        IrrlichtDevice *device;
        IVideoDriver *driver;
        IGUIEnvironment *guienv;

        MyEventReceiver *receiver;

        int tourJoueur;


        bool isGagne();
};

#endif // GAME_H
