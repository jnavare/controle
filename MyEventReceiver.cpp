#include "MyEventReceiver.h"
#include "game.h"
#include <iostream>
#include <sstream>
#include <string.h>

MyEventReceiver::MyEventReceiver(Game *pendu)
{
    //ctor
    this->pendu = pendu;
}

MyEventReceiver::~MyEventReceiver()
{
    //dtor
}

bool MyEventReceiver::OnEvent(const SEvent& event)
{
    if (event.EventType == EET_GUI_EVENT)
    {
        s32 id = event.GUIEvent.Caller->getID();

        switch(event.GUIEvent.EventType)
        {
        case EGET_BUTTON_CLICKED:

            switch(id)
            {
            //pression sur le bouton "D�marrer une partie de pendu" r�initilisation du mot myst�re et du nombre d'essais
            case GUI_ID_DEBUTPARTIE:

                pendu->debutJeu();

                pendu->gui->labelEssais->setVisible(true);
                pendu->gui->labelNbEssais->setVisible(true);

                pendu->gui->labelChoixLettre->setVisible(true);
                pendu->gui->choixLettre->setVisible(true);
                pendu->gui->btnValider->setVisible(true);

                //pendu->gui->labelMotMystere->setText(pendu->gui->labelNbEssais->getText()); //fonctionne



                pendu->gui->labelMotMystere->setVisible(true);

                return true;

            //pression sur le bouton valider (test de la valeur saisie dans l'editBox
            case GUI_ID_VALIDER:
                //todo limiter la saisie � un �l�ment
                //verifier si le caract�re saisi est bine une lettre

                std::cout << "Le joueur a choisi la lettre "  <<std::endl;

                pendu->joueLettre();

                //R�activer l'affichage du mot mystere pour le r�actualiser
                pendu->gui->labelMotMystere->setVisible(true);

                return true;
            default:
                return false;
            }
            break;
        default:
            break;
        }
    }
    return false;
}
