#include "Game.h"

#include "GameGUI.h"



using namespace std;
Game::Game()
{
    //ctor
    //Cr�er le device irrlicht
    device = createDevice(EDT_SOFTWARE, dimension2d<u32>(800, 400), 16,
            false, false, false, 0);

    driver = device->getVideoDriver();
    guienv = device->getGUIEnvironment();

    //Cr�ation de la classe GUI
    gui = new GameGUI(guienv);

    //Event receiver
    receiver = new MyEventReceiver(this);

    //Lien entre le Device et l'event receiver
    device->setEventReceiver(receiver);

    //initialisation du nombre d'essais � 7
    nbEssais=7;

    //initialisation du booleen choixLettre Gagnant
    choixLettreGagnant = false;

    //impossible de charger � partir d'un fichier le mot � deviner donc m�thode alternative
    idMotMystere = rand()%2+1;
    switch (idMotMystere)
    {
    case 1:
        motMystere = "troll";
        break;
    case 2:
        motMystere = "schtroumph";
        break;
    case 3:
        motMystere = "farfadet";
        break;
    }
    //motMystere = "troll";
    std::cout << "Le mot mystere fait " << motMystere.size() << " caracteres" << std::endl;
}

Game::~Game()
{
    //dtor
    device->drop();
}

void Game::run() {
    //Boucle de rendu principale
    while (device->run())
    {
        driver->beginScene(true, true, SColor(0,200,200,200));

        guienv->drawAll();

        driver->endScene();
    }
}

void Game::debutJeu()
{
    std::cout << "Une partie de pendu demarre" << std::endl;

    //Lecture du fichier
    std::ifstream fichier("ListeMotsMysteres.txt", ios::in);

    // tentative d'ouverture du fichier
    if(fichier)
    {
        // si ouverture r�ussie
        std::string mot1,mot2,mot3,mot4,mot5;  // d�claration de 5chaines qui contiendront chaun une ligne
        fichier >>  mot1 >> mot2 >> mot3 >> mot4 >> mot5;
        //getline(fichier, mot1);  // on met dans "mot1" la ligne

        fichier.close();  // on ferme le fichier
    }
    else  // sinon
        cerr << "Impossible d'ouvrir le fichier !" << endl;
}


void Game::joueLettre() {

    //Verifie si la lettre est pr�sente dans le mot en r�cup�rant la taille du mot avec .size
    // et en comparant chaque lettre du mot avec la lettre entr�e par le joueur
    for (unsigned i = 0 ; i < motMystere.size() ; ++i)
    {
        //motMystere[i];
        //si la lettre est trouv�e le booleen choixLettreGagnant passe � true
    }

    //
    if (choixLettreGagnant)
    {
    std::cout << "La lettre presente dans le mot" << std::endl;
    //affichage de la lettre dans le labelmotmystere
    }
    else
    {
        std::cout << "La lettre absente du mot" << std::endl;
        //Perte d'un essai
        nbEssais -= 1;
        std::cout << "il vous reste " << nbEssais << " essais" << std::endl;
        if (nbEssais<=0)
        {
            std::cout << "La partie est terminee" << std::endl;
            std::cout << "Il fallait deviner le mot : " << motMystere << std::endl;

        }
        /*if(this->isGagne) // si toutes les lettres ont �t� trouv�es
        {
            std::cout << "Le mot mystere " << motMystere <<" a ete trouve en " << nbEssais << " essais" << std::endl;
        }*/
    }
}


bool Game::isGagne() {
    return false;
}
