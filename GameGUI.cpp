#include "GameGUI.h"


GameGUI::GameGUI(IGUIEnvironment *guienv)
{
    //ctor
    this->guienv = guienv;

    fenetreJeu = guienv->addStaticText(L"",
        rect<int>(0,0,800,400));

    btnDebutPartie = guienv->addButton(rect<int>(10,10,120+100,10+20), fenetreJeu, GUI_ID_DEBUTPARTIE, L"Demarrer une partie de pendu");
    btnValider = guienv->addButton(rect<int>(250,70,250+100,70+20), fenetreJeu, GUI_ID_VALIDER, L"Valider");

    btnValider->setVisible(false);

    //Affichage du nombre d'essais restants
    labelEssais = guienv->addStaticText(L"Nombre d'essais restants :",
        rect<int>(10,40,10+100,40+20), false, false, fenetreJeu);
    labelEssais->setVisible(false);

    labelNbEssais = guienv->addStaticText(L"7",
        rect<int>(120,40,10+120,40+20), false, false, fenetreJeu);
    labelNbEssais->setVisible(false);


    //Choix de lettre
    labelChoixLettre = guienv->addStaticText(L"Quelle lettre choisissez-vous?",
        rect<int>(10,70,10+120,70+20), false, false, fenetreJeu);
    labelChoixLettre->setVisible(false);


    choixLettre = guienv->addEditBox(L"", rect<int>(140,70,140+100,70+20), true, fenetreJeu);
    choixLettre->setVisible(false);

    labelMotMystere = guienv->addStaticText(L"",
        rect<int>(40,110,40+120,110+20), false, false, fenetreJeu);
    labelMotMystere->setVisible(false);

}



GameGUI::~GameGUI()
{
    //dtor
}
